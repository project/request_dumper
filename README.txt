Request Dumper

This module allows you to dump selected requests to files in a temp or
private directory.

This can be useful for debugging API requests, form submit issues, etc.

Be careful to cleanup files manually since the dumped headers may contain
session cookies or other sensitive data.

Note that the page cache may respond before the request reaches the dumper.
If you need to capture all anonymous requests, disable the page cache.
