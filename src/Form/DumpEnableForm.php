<?php

namespace Drupal\request_dumper\Form;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Provides a form for enabling request dumping.
 */
class DumpEnableForm implements FormInterface, ContainerInjectionInterface {

  use DependencySerializationTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a RequestDumper object.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $stream_wrapper_manager
   *   The stream wrapper manager.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $translation
   *   The string translation service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger channel.
   */
  public function __construct(StateInterface $state, TimeInterface $time, DateFormatterInterface $date_formatter, StreamWrapperManagerInterface $stream_wrapper_manager, FileSystemInterface $file_system, TranslationInterface $translation, MessengerInterface $messenger, LoggerInterface $logger) {
    $this->state = $state;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->fileSystem = $file_system;
    $this->setStringTranslation($translation);
    $this->setMessenger($messenger);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('stream_wrapper_manager'),
      $container->get('file_system'),
      $container->get('string_translation'),
      $container->get('messenger'),
      $container->get('logger.channel.request_dumper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'request_dumper_enable_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $values = $this->state->get('request_dumper.enable', []);
    $form['original_values'] = [
      '#type' => 'value',
      '#value' => $values,
    ];
    $end_time = count($values) > 1 && !empty($values['end_time']) ? (int) $values['end_time'] : 0;
    $title = $end_time >= $this->time->getRequestTime() ? $this->t('Current end time') : $this->t('Previous end time');
    $form['current_end_time'] = [
      '#type' => 'item',
      '#title' => $title,
      '#markup' => $end_time ? $this->dateFormatter->format($end_time, 'custom', 'Y-m-d H:i:s T') : $this->t('Disabled'),
      '#description' => $this->t('If currently active, the time the dumping will end.'),
      '#access' => $end_time > 0,
    ];
    $path = $values['always_path'] ?? '';
    $token = '';
    if ($path) {
      $parts = explode('/', $path);
      $token = end($parts);
    }

    $form['enable_always_capture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Always-on Dump URL'),
      '#default_value' => (bool) $token,

      '#description' => $token ? $this->t('Use this path to capture or debug webhook data:') : $this->t('Enables a path to capture or debug webhook data.'),
    ];
    $form['dump_path'] = [
      '#type' => 'link',
      '#title' => $path,
      '#url' => Url::fromRoute('request_dumper.always_dump', ['token' => $token]),
      '#access' => (bool) $token,
    ];

    $intervals = [0, 60, 90, 120, 300, 600, 900, 1800];
    $period = array_combine($intervals, array_map([$this->dateFormatter, 'formatInterval'], $intervals));
    $period[0] = $this->t('Disable');
    $form['end_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Enable request dumping for duration'),
      '#default_value' => 0,
      '#options' => $period,
      '#description' => $this->t('Duration after form submission to enable request dumping.'),
    ];

    // Any writeable wrapper can potentially be used for the dump files
    // directory, including a remote file system, but excluding public.
    $options = $this->streamWrapperManager->getDescriptions(StreamWrapperInterface::HIDDEN);
    unset($options['public']);
    $form['file_scheme'] = [
      '#type' => 'radios',
      '#title' => $this->t('Dump file location'),
      '#default_value' => $values['file_scheme'] ?? 'temporary',
      '#options' => $options,
      '#description' => $this->t('The file location where requests will be dumped.'),
    ];

    $method_options = [
      'POST',
      'PATCH',
      'GET',
      'PUT',
      'DELETE',
    ];

    $form['dump_methods'] = [
      '#type' => 'checkboxes',
      '#options' => array_combine($method_options, $method_options),
      '#title' => $this->t('Dump which methods'),
      '#default_value' => (array) ($values['dump_methods'] ?? []),
    ];

    $path_prefix = $values['path_prefix'] ?? '';
    $form['path_prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path prefix'),
      '#default_value' => $path_prefix,
      '#description' => $this->t('Optionally, only dump requests matching this path prefix, e.g. /jsonapi/node/'),
    ];

    $form['cleanup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Cleanup existing files'),
      '#description' => $this->t('Delete all existing dump files in the selected location.'),
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->cleanValues()->getValues();
    $cleanup = !empty($values['cleanup']);
    $original_values = $values['original_values'];
    unset($values['cleanup'], $values['original_values']);
    $values['dump_methods'] = array_filter($values['dump_methods']);
    unset($values['current_end_time']);
    $duration = '';
    if ($values['end_time'] > 0) {
      $duration = $this->dateFormatter->formatInterval($values['end_time']);
      $values['end_time'] += $this->time->getCurrentTime();
    }
    $values['path_prefix'] = trim($values['path_prefix']);
    if ($values['path_prefix']) {
      // Make sure there is a single leading slash.
      $values['path_prefix'] = '/' . ltrim($values['path_prefix'], '/');
    }
    if ($values['enable_always_capture']) {
      if (empty($original_values['always_path'])) {
        // @todo: Get the path from the route.
        $original_values['always_path'] = '/request-dumper/always/' . Crypt::hashBase64(random_bytes(16));
      }
      $values['always_path'] = $original_values['always_path'];
      $this->messenger()->addMessage($this->t('Always-on Dump URL Enabled'));
    }
    else {
      $values['always_path'] = '';
    }
    unset($values['enable_always_capture']);
    $this->state->set('request_dumper.enable', $values);
    if ($values['end_time'] > 0) {
      $message = 'Request dumper enabled for @duration for methods @methods and path prefix "@path_prefix"';
      $args = [
        '@duration' => $duration,
        '@methods' => implode(', ', $values['dump_methods']),
        '@path_prefix' => $values['path_prefix'],
      ];
      $this->messenger()->addMessage($this->t($message, $args));
      $this->logger->notice($message, $args);
    }
    else {
      $this->messenger()->addMessage($this->t('Request dumping is disabled.'));
    }
    if ($cleanup) {
      $path = $values['file_scheme'] . '://request_dumper';
      $this->fileSystem->deleteRecursive($path);
      $this->messenger()->addMessage($this->t('All files deleted from @path.', ['@path' => $this->fileSystem->realpath($path)]));
    }
  }

}
