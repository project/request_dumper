<?php

namespace Drupal\request_dumper\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AlwaysDump extends ControllerBase {

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Controller constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * Builds a result.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response object with JSON content.
   */
  public function resultPage(): Response {
    $content = '{"result":"success"}';
    $headers = [
      'Content-Type' => 'application/json',
    ];
    return new Response($content, 200, $headers);
  }

  /**
   * Access check.
   *
   * @param string $token
   *   The token in the path.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(string $token): AccessResult {
    $values = $this->state->get('request_dumper.enable', []);
    $access = $token && !empty($values['always_path']);
    if ($access) {
      $parts = explode('/', $values['always_path']);
      $expected_token = end($parts);
    }
    return AccessResult::allowedIf($access && hash_equals($expected_token, $token));
  }

}
