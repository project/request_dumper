<?php

namespace Drupal\request_dumper\StackMiddleware;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides support for dumping requests.
 */
class RequestDumper implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $app;

  /**
   * The state key/value store.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a RequestDumper object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key/value store.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file handler.
   */
  public function __construct(HttpKernelInterface $http_kernel, StateInterface $state, FileSystemInterface $file_system) {
    $this->app = $http_kernel;
    $this->state = $state;
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = 1, $catch = TRUE): Response {
    $values = $this->state->get('request_dumper.enable', []);
    $end_time = count($values) > 1 && !empty($values['end_time']) ? (int) $values['end_time'] : 0;
    $path = $request->getPathInfo();
    $always_capture = !empty($values['always_path']) && $path === $values['always_path'];
    // This behavior should only be enabled for a short period of time.
    if ($always_capture || ($end_time && $end_time >= $request->server->get('REQUEST_TIME'))) {
      try {
        $dump_methods = (array) ($values['dump_methods'] ?? []);
        $path_prefix = $values['path_prefix'] ?? '';
        $file_scheme = $values['file_scheme'] ?? 'temporary';
        $method = $request->getMethod();
        if ($always_capture || (in_array($method, $dump_methods, TRUE) && (!$path_prefix || strpos($path, $path_prefix) === 0))) {
          $path = $file_scheme . '://request_dumper' . $path;
          $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
          $time = $request->server->get('REQUEST_TIME_FLOAT');
          $uri = $path . '/' . $method . '-' . $time . '-content.txt';
          $data = $request->getContent();
          $this->fileSystem->saveData($data, $uri, FileSystemInterface::EXISTS_REPLACE);
          $uri = $path . '/' . $method . '-' . $time . '-headers.txt';
          $headers = $request->headers->all();
          $headers['RequestUri'] = $request->getRequestUri();
          $headers['DecodedRequestUri'] = rawurldecode($headers['RequestUri']);
          $data = var_export($headers, TRUE);
          $this->fileSystem->saveData($data, $uri, FileSystemInterface::EXISTS_REPLACE);
        }
      }
      catch (\Exception $e) {
        // Ignore it.
      }
    }
    return $this->app->handle($request, $type, $catch);
  }

}
